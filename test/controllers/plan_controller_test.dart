import 'package:flutter_test/flutter_test.dart';
import 'package:week5_519h0077/controllers/plan_controller.dart';

void main() {
  test('Test addNewPlan', () {
    var controller = PlanController();
    controller.addNewPlan('A');

    expect(1, controller.plans.length);
    expect('A', controller.plans.first.name);
  });

  test('Test addNewPlan with Vietnamese name', () {
    var controller = PlanController();
    controller.addNewPlan('Tạo ứng dụng Flutter');

    expect(1, controller.plans.length);
    expect('Tạo ứng dụng Flutter', controller.plans.first.name);
  });

}
