import 'package:flutter/material.dart';
import 'package:week5_519h0077/views/plan_creator_screen.dart';


class MyApp extends StatelessWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Todo App",
      home: Scaffold(
        body: PlanCreatorScreen(),
      ),
    );
  }
}